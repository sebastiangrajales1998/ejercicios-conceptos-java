package ejercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Haz una aplicación que calcule el área de un círculo(pi*R2). El radio se pedirá porteclado
        (recuerda  pasar  de  String  a  double  conDouble.parseDouble).  Usa  la constante PI y el método pow
        de Math. */

        Double radio;
        Double area;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese el radio del circulo: ");
        radio = Double.parseDouble(bufferedReader.readLine());

        area = Math.PI * (Math.pow(radio, 2));

        System.out.println("El area del circulo es: " + area);
    }
}
