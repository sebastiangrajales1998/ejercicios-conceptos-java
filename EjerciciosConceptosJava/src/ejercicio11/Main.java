package ejercicio11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Realizar  la  construcción  de  un  algoritmo  que  permita  de  acuerdo  a  una  frase pasada
         por  consola,  indicar  cual  es  la  longitud  de  esta  frase,  adicionalmente cuantas vocales
         tiene de “a,e,i,o,u”. */

        String frase;
        int numeroEspacios = 0;
        int numeroVocales = 0;
        char[] vocales = {'a', 'e', 'i', 'o', 'u'};
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese la frase: ");
        frase = bufferedReader.readLine().toLowerCase();

        for (int i = 0; i < frase.length(); i++) {
            if (frase.charAt(i) == ' ') {
                numeroEspacios++;
            }

            for (int j = 0; j < vocales.length; j++) {
                if (frase.charAt(i) == vocales[j]) {
                    numeroVocales++;
                }
            }
        }

        System.out.println("Longitud frase contando espacios: " + frase.length());
        System.out.println("Longitud frase sin contar espacios: " + (frase.length() - numeroEspacios));
        System.out.println("Número de vocales en la frase: " + numeroVocales);
    }
}
