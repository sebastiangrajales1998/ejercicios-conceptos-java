package ejercicio7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Lee un número por teclado y comprueba que este numero es mayor o igual que cero,
        si no lo es lo volverá a pedir (do while), después muestra ese número por consola. */

        int numero;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.print("Ingrese un número: ");
            numero = Integer.parseInt(bufferedReader.readLine());

            if (numero >= 0) {
                System.out.println("El número " + numero + " >= 0");
                break;
            } else {
                System.out.println("El número " + numero + " < 0, intente de nuevo");
            }
        } while (true);
    }
}
