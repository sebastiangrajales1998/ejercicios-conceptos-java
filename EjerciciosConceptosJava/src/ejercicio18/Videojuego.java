package ejercicio18;

public class Videojuego implements Entregable {
    private static final int HORAS_ESTIMADAS_DEFECTO = 10;
    private static final boolean ENTREGADO_DEFECTO = false;
    private String titulo;
    private int horasEstimadas;
    private boolean entregado;
    private String genero;
    private String compania;

    public Videojuego(String titulo, int horasEstimadas, String genero, String compania) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
        this.genero = genero;
        this.compania = compania;
        this.entregado = ENTREGADO_DEFECTO;
    }

    public Videojuego(String titulo, int horasEstimadas) {
        this(titulo, horasEstimadas, "", "");
    }

    public Videojuego() {
        this("", HORAS_ESTIMADAS_DEFECTO);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(int horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    @Override
    public String toString() {
        return "Videojuego{" +
                "titulo='" + titulo + '\'' +
                ", horasEstimadas=" + horasEstimadas +
                ", entregado=" + entregado +
                ", genero='" + genero + '\'' +
                ", compania='" + compania + '\'' +
                '}';
    }

    @Override
    public void entregar() {
        this.entregado = true;
    }

    @Override
    public void devolver() {
        this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return entregado;
    }

    @Override
    public boolean compareTo(Object a) {
        // Retorna true si tiene mayor número de horas estimadas
        Videojuego videojuego = (Videojuego) a;

        if (this.getHorasEstimadas() > videojuego.getHorasEstimadas()) {
            return true;
        } else {
            return false;
        }
    }
}
