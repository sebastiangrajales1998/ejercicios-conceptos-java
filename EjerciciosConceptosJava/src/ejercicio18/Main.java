package ejercicio18;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Serie[] series = new Serie[5];
        Serie serieMayorTemporadas;
        Videojuego videojuegoMasHoras;
        Videojuego[] videojuegos = new Videojuego[5];
        ArrayList<Serie> seriesEntregadas = new ArrayList<>();
        ArrayList<Videojuego> videojuegosEntregados = new ArrayList<>();


        // Series
        series[0] = new Serie();
        series[1] = new Serie("Stranger Things", "Ross Duffer");
        series[2] = new Serie("Making a Murderer", 2, "Documental", "Laura Ricciardi");
        series[3] = new Serie("The Magicians", 5, "Fantasía", "Sera Gamble");
        series[4] = new Serie("Yellowstone", 3, "Drama", "Taylor Sheridan");

        // Videojuegos
        videojuegos[0] = new Videojuego();
        videojuegos[1] = new Videojuego("Cyberpunk 2077", 30);
        videojuegos[2] = new Videojuego("Need for Speed: Most Wanted", 56, "Carreras", "Electronic Arts");
        videojuegos[3] = new Videojuego("Assassin's Creed: Brotherhood", 24, "Aventura", "Ubisoft");
        videojuegos[4] = new Videojuego("Sniper: Ghost Warrior", 12, "Tactical shooter", "City Interactive");

        // Entregar
        series[1].entregar();
        series[3].entregar();

        videojuegos[0].entregar();
        videojuegos[2].entregar();
        videojuegos[4].entregar();

        // Inicializa para la comparacion
        serieMayorTemporadas = series[0];
        videojuegoMasHoras = videojuegos[0];

        for (int i = 0; i < series.length; i++) {
            if (series[i].isEntregado()) {
                seriesEntregadas.add(series[i]);
            }

            if (series[i].compareTo(serieMayorTemporadas)) {
                serieMayorTemporadas = series[i];
            }

            if (videojuegos[i].isEntregado()) {
                videojuegosEntregados.add(videojuegos[i]);
            }

            if (videojuegos[i].compareTo(videojuegoMasHoras)) {
                videojuegoMasHoras = videojuegos[i];
            }

        }

        // Mostrar resultados
        System.out.println("Series entregadas");
        seriesEntregadas.forEach(serie -> System.out.println(serie.toString()));

        System.out.println("Videojuegos entregados");
        videojuegosEntregados.forEach(videojuego -> System.out.println(videojuego.toString()));

        System.out.println("Serie con más temporadas");
        System.out.println(serieMayorTemporadas.toString());

        System.out.println("Videojuego con más horas estimadas");
        System.out.println(videojuegoMasHoras.toString());
    }
}
