package ejercicio12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Pedir dos palabras por teclado, indicar si son iguales, sino son iguales mostrar sus diferencias. */

        String primerPalabra;
        String segundaPalabra;
        String diferencias = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese la primer palabra: ");
        primerPalabra = bufferedReader.readLine();

        System.out.print("Ingrese la segunda palabra: ");
        segundaPalabra = bufferedReader.readLine();

        if (primerPalabra.equals(segundaPalabra)) {
            System.out.println("Las palabras son iguales");
        } else {
            String palabraMayorLongitud;
            String palabraMenorLongitud;

            if (primerPalabra.length() >= segundaPalabra.length()) {
                palabraMayorLongitud = primerPalabra;
                palabraMenorLongitud = segundaPalabra;
            } else {
                palabraMayorLongitud = segundaPalabra;
                palabraMenorLongitud = primerPalabra;
            }

            for (int i = 0; i < palabraMayorLongitud.length(); i++) {
                if (i < palabraMenorLongitud.length()) {
                    if (palabraMayorLongitud.charAt(i) != palabraMenorLongitud.charAt(i)) {
                        diferencias = diferencias.concat("{" + palabraMayorLongitud.charAt(i)
                        + ", " + palabraMenorLongitud.charAt(i) + "}");
                    }
                } else {
                    diferencias = diferencias.concat("{" + palabraMayorLongitud.charAt(i) + ", }");
                }
            }

            System.out.println("Diferencias:");
            System.out.println(diferencias);
        }
    }
}
