package ejercicio8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Crea una aplicación por consola que nos pida un día de la semana y que nos diga si es un día laboral o no.
        Usa un switch para ello. */

        int diaSemana;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingresa un día de la semana (1 a 7): ");
        diaSemana = Integer.parseInt(bufferedReader.readLine());

        switch (diaSemana) {
            case 1: {
                System.out.println("El día lunes es laboral");
                break;
            }
            case 2: {
                System.out.println("Es día martes es laboral");
                break;
            }
            case 3: {
                System.out.println("Es día miercoles es laboral");
                break;
            }
            case 4: {
                System.out.println("Es día jueves es laboral");
                break;
            }
            case 5: {
                System.out.println("Es día viernes es laboral");
                break;
            }
            case 6: {
                System.out.println("Es día sabado no es laboral");
                break;
            }
            case 7: {
                System.out.println("Es día domingo no es laboral");
                break;
            }
            default: {
                System.out.println("Número de día inválido");
                break;
            }
        }
    }
}
