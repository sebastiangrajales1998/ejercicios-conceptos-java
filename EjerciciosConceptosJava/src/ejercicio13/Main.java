package ejercicio13;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        /* Realizar un algoritmo que permita consulta la fecha y hora actual en el
        formato (AAAA/MM/DD) (HH:MM:SS) */

        Calendar calendar = new GregorianCalendar();
        String anno = String.valueOf(calendar.get(Calendar.YEAR));
        String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String hora = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minutos = String.valueOf(calendar.get(Calendar.MINUTE));
        String segundos = String.valueOf(calendar.get(Calendar.SECOND));

        if (mes.length() == 1) {
            mes = "0" + mes;
        }

        if (dia.length() == 1) {
            dia = "0" + dia;
        }

        String fecha = "(" + anno + "/" + mes + "/" + dia + ") (" + hora + ":" + minutos + ":" + segundos + ")";

        System.out.println(fecha);
    }
}
