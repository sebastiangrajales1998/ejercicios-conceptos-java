package ejercicio16;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        String nombre;
        int edad;
        char sexo;
        float peso;
        float altura;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese el nombre: ");
        nombre = bufferedReader.readLine();

        System.out.print("Ingrese la edad: ");
        edad = Integer.parseInt(bufferedReader.readLine());

        System.out.print("'H' para hombre, 'M' para mujer\nIngrese el sexo: ");
        sexo = bufferedReader.readLine().charAt(0);

        System.out.print("Ingrese el peso (kg): ");
        peso = Float.parseFloat(bufferedReader.readLine());

        System.out.print("Ingrese la altura (m): ");
        altura = Float.parseFloat(bufferedReader.readLine());

        Persona primerPersona = new Persona(nombre, edad, sexo, peso, altura);
        Persona segundaPersona = new Persona("Segunda Persona", 17, 'M');
        Persona tercerPersona = new Persona();

        tercerPersona.setNombre("Tercer Persona");
        tercerPersona.setEdad(22);
        tercerPersona.setPeso(72f);
        tercerPersona.setAltura(1.72f);

        mostarMensajePesoIdeal(primerPersona);
        mostrarMensajeMayorEdad(primerPersona);
        System.out.println(primerPersona.toString() + "\n");

        mostarMensajePesoIdeal(segundaPersona);
        mostrarMensajeMayorEdad(segundaPersona);
        System.out.println(segundaPersona.toString() + "\n");

        mostarMensajePesoIdeal(tercerPersona);
        mostrarMensajeMayorEdad(tercerPersona);
        System.out.println(tercerPersona.toString() + "\n");
    }

    public static void mostarMensajePesoIdeal(Persona persona) {
        final String MUY_BAJO = " tu peso está muy bajo";
        final String BAJO = " tu peso está bajo, pero nada alarmante";
        final String SOBREPESO = " tienes sobrepeso";
        String mensaje;

        switch (persona.calcularIMC()) {
            case -1: {
                mensaje = MUY_BAJO;
                break;
            }
            case 0: {
                mensaje = BAJO;
                break;
            }
            case 1: {
                mensaje = SOBREPESO;
                break;
            }
            default: {
                mensaje = " se ingresaron mal las métricas";
                break;
            }
        }

        System.out.println(persona.getNombre() + mensaje);
    }

    public static void mostrarMensajeMayorEdad(Persona persona) {
        final String MAYOR_EDAD = " eres mayor de edad";
        final String MENOR_EDAD = " eres menor de edad";
        String mensaje;

        if (persona.esMayorDeEdad()) {
            mensaje = MAYOR_EDAD;
        } else {
            mensaje = MENOR_EDAD;
        }

        System.out.println(persona.getNombre() + mensaje);
    }
}
