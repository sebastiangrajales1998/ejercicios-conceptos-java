package ejercicio16;

public class Persona {
    private final static char HOMBRE = 'H';
    private String nombre;
    private int edad;
    private String dni;
    private char sexo;
    private float peso;
    private float altura;

    public Persona(String nombre, int edad, char sexo, float peso, float altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = this.generaDNI();
        this.sexo = this.comprobarSexo(sexo);
        this.peso = peso;
        this.altura = altura;
    }

    public Persona(String nombre, int edad, char sexo) {
        this(nombre, edad, sexo, 0.0f, 0.0f);
    }

    public Persona() {
        this("", 0, HOMBRE);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public int calcularIMC() {
        final int MUY_BAJO = -1;
        final int BAJO = 0;
        final int SOBREPESO = 1;
        final int ERROR = -2;
        float imc =  this.getPeso() / ((float) Math.pow(this.getAltura(), 2));

        if ((this.getPeso() <= 0.0f) || (this.getAltura() <= 0.0f)) {
            return ERROR;
        }

        if (imc < 20) {
            return MUY_BAJO;
        } else if ((imc >= 20) && (imc <= 25)) {
            return BAJO;
        } else {
            return SOBREPESO;
        }
    }

    public boolean esMayorDeEdad() {
        if (this.getEdad() >= 18) {
            return true;
        } else {
            return false;
        }
    }

    private char comprobarSexo(char sexo) {
        if (sexo == 'M') {
            return sexo;
        } else {
            return HOMBRE;
        }
    }

    private String generaDNI() {
        int cifras;
        int posicionLetra;
        final char[] letras = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B',
                'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};

        while (true) {
            cifras = (int) (100000000 * Math.random());
            posicionLetra = cifras % 23;

            if (posicionLetra < letras.length) {
                return String.valueOf(letras[posicionLetra]) + cifras;
            }
        }
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", dni='" + dni + '\'' +
                ", sexo=" + sexo +
                ", peso=" + peso +
                ", altura=" + altura +
                '}';
    }
}
