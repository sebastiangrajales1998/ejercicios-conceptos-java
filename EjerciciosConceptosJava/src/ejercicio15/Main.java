package ejercicio15;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /*
        Hacer un programa que muestre el siguiente menú de opciones
        ****** GESTION CINEMATOGRÁFICA ********
        1-NUEVO ACTOR
        2-BUSCAR ACTOR
        3-ELIMINAR ACTOR
        4-MODIFICAR ACTOR
        5-VER TODOS LOS ACTORES
        6-VER PELICULAS DE LOS ACTORES
        7-VER CATEGORIA DE LAS PELICULAS DE LOS ACTORES
        8-SALIR
        EL SISTEMA SOLO VA A SALIR CUANDO SE DIGITE EL NUMERO 8, MIENTRAS SE DIGITE UNA DE LAS CINCO OPCIONES
        DEBE SEGUIR MOSTRADO EL MENU Y SI EL USUARIO DIGITA UN NUMERO QUE NO ESTA EN EL MENU SE DEBE ARROJAR
        UN MENSAJE " OPCION INCORRECTO". Y MOSTRAR EL MENU NUEVAMENTE.PISTA: CONVINAR SWICHT Y ALGUNO DE LOS BUCLES.
        */

        int opcion;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.print(
                    "****** GESTION CINEMATOGRÁFICA ********\n" + "1-NUEVO ACTOR\n2-BUSCAR ACTOR\n" +
                    "3-ELIMINAR ACTOR\n4-MODIFICAR ACTOR\n5-VER TODOS LOS ACTORES\n" +
                    "6-VER PELICULAS DE LOS ACTORES\n7-VER CATEGORIA DE LAS PELICULAS DE LOS ACTORES\n" +
                    "8-SALIR\nIngrese opcion:"
            );
            opcion = Integer.parseInt(bufferedReader.readLine());

            if (opcion == 8) {
                System.out.println("Saliendo...");
                break;
            } else {
                switch (opcion) {
                    case 1: {
                        System.out.println("Se creó actor");
                        break;
                    }
                    case 2: {
                        System.out.println("Se buscó actor");
                        break;
                    }
                    case 3: {
                        System.out.println("Se eliminó actor");
                        break;
                    }
                    case 4: {
                        System.out.println("Se modificó actor");
                        break;
                    }
                    case 5: {
                        System.out.println("Todos los actores->");
                        break;
                    }
                    case 6: {
                        System.out.println("Se mostraron peliculas del actor");
                        break;
                    }
                    case 7: {
                        System.out.println("Se mostraron categorias");
                        break;
                    }
                    default: {
                        System.out.println("Opción inválida, intente de nuevo");
                        break;
                    }
                }
            }
        } while (true);
    }
}
