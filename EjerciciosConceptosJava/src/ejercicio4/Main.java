package ejercicio4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Lee  un  número  por  teclado  que  pida  el  precio  de  un  producto  (puede  tener decimales) y
        calcule el precio final con IVA. El IVA sera una constante que sera del 21%. */

        final Double IVA = 0.21;
        Double precioProducto;
        Double precioFinal;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese el precio del producto: ");
        precioProducto = Double.parseDouble(bufferedReader.readLine());

        precioFinal = precioProducto + (precioProducto * IVA);

        System.out.println("Precio final del producto: " + precioFinal);
    }
}
