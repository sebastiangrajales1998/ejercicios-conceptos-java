package ejercicio14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Crear un programa que pida un numero por teclado y que imprima por pantalla los números desde el
        numero introducido hasta 1000 con saldos de 2 en 2. */

        int numero;
        final int META = 1000;
        boolean esMenorQueMeta;

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese el número: ");
        numero = Integer.parseInt(bufferedReader.readLine());

        if (numero < META) {
            esMenorQueMeta = true;
        } else {
            esMenorQueMeta = false;
        }

        while (true) {
            System.out.println(numero);

            if ((numero < META) && esMenorQueMeta) {
                if ((numero += 2) > 1000) {
                    break;
                }
            } else {
                if ((numero -= 2) < 1000) {
                    break;
                }
            }
        }

        System.out.println("Se ha llegado a " + META);
    }
}
