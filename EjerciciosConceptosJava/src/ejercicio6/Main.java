package ejercicio6;

public class Main {
    public static void main(String[] args) {
        /* Realiza el ejercicio anterior usando un ciclo for. */

        boolean primeraVuelta = true;

        for (int i = 1; i <= 100; i++) {
            if (i == 1) {
                if (primeraVuelta) {
                    System.out.println("Numeros pares:");
                } else {
                    System.out.println("Numeros impares:");
                }
            }

            if (((i % 2) == 0) && primeraVuelta) {
                System.out.println(i);

                if (i == 100) {
                    primeraVuelta = false;
                    i = 0;
                }
            } else if (((i % 2) == 1) && !primeraVuelta) {
                System.out.println(i);
            }
        }
    }
}
