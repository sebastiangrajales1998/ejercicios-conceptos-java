package ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Al ejercicio anterior agregar entrada por consola de dos valores e indicar si son mayores, menores
        o iguales. */

        int primerNumero;
        int segundoNumero;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese el primer número: ");
        primerNumero = Integer.parseInt(bufferedReader.readLine());

        System.out.print("Ingrese el segundo número: ");
        segundoNumero = Integer.parseInt(bufferedReader.readLine());

        if (primerNumero > segundoNumero) {
            System.out.println("El primer número es mayor, " + primerNumero + " > " + segundoNumero);
        } else if (primerNumero < segundoNumero) {
            System.out.println("El segundo número es mayor, " + primerNumero + " < " + segundoNumero);
        } else {
            System.out.println("Ambos números son iguales, " + primerNumero + " == " + segundoNumero);
        }

    }
}
