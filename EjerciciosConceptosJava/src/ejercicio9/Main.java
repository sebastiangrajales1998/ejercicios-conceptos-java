package ejercicio9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Del texto, “La sonrisa sera la mejor arma contra la tristeza” Reemplaza  todas las a del  String  anterior
        por  una e,  adicionalmente  concatenar  a  esta  frase  una adicional que ustedes quieran incluir por
        consola y las muestre luego unidas. */

        final String TEXTO = "La sonrisa sera la mejor arma contra la tristeza";
        final char ORIGINAL = 'a';
        final char REEMPLAZO = 'e';
        String textoModificado;
        String textoEntrada;
        String textoConcatenado;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        textoModificado = TEXTO.replace(ORIGINAL, REEMPLAZO);
        System.out.println(textoModificado);

        System.out.print("Ingrese el texto a concatenar: ");
        textoEntrada = bufferedReader.readLine();

        textoConcatenado = textoModificado.concat(textoEntrada);
        System.out.println(textoConcatenado);
    }
}
