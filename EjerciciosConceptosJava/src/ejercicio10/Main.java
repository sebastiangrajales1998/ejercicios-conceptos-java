package ejercicio10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        /* Realizar una aplicación de consola, que al ingresar una frase por teclado elimine los espacios
        que esta contenga. */

        String frase;
        String fraseSinEspacios;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Ingrese la frase: ");
        frase = bufferedReader.readLine();

        fraseSinEspacios = frase.replace(" ", "");

        System.out.println(fraseSinEspacios);
    }
}
