package ejercicio5;

public class Main {
    public static void main(String[] args) {
        /* Muestra los números impares y pares del 1 al 100 (ambos incluidos). Usa un bucle while. */

        int numero = 1;
        boolean primeraVuelta = true;

        while ((numero <= 100) || primeraVuelta) {
            if (numero == 1) {
                if (primeraVuelta) {
                    System.out.println("Numeros pares:");
                } else {
                    System.out.println("Numeros impares:");
                }
            }

            if (((numero % 2) == 0) && primeraVuelta) {
                System.out.println(numero);

                if (numero == 100) {
                    primeraVuelta = false;
                    numero = 0;
                }
            } else if (((numero % 2) == 1) && !primeraVuelta) {
                System.out.println(numero);
            }

            numero++;
        }
    }
}
