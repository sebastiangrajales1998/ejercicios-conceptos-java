package ejercicio17;

public class Main {
    public static void main(String[] args) {
        int precioElectrodomesticos = 0;
        int precioLavadoras = 0;
        int precioTelevisiones = 0;
        Electrodomestico[] electrodomesticos = new Electrodomestico[10];

        electrodomesticos[0] = new Electrodomestico();
        electrodomesticos[1] = new Electrodomestico(10, 50);
        electrodomesticos[2] = new Electrodomestico(20, "negro", 'D', 45);
        electrodomesticos[3] = new Lavadora();
        electrodomesticos[4] = new Lavadora(15, 55);
        electrodomesticos[5] = new Lavadora(20, "ROJO", 'E', 30.5f, 35);
        electrodomesticos[6] = new Television();
        electrodomesticos[7] = new Television(35, 10);
        electrodomesticos[8] = new Television(29, "Gris", 'A', 15, 45, true);
        electrodomesticos[9] = new Television(15, "AZUL", 'L', 10, 15, false);

        for (int i = 0; i < electrodomesticos.length; i++) {
            System.out.println(electrodomesticos[i].getClass().getName() + "\t" + electrodomesticos[i].precioFinal());
            if (electrodomesticos[i] instanceof Lavadora) {
                precioLavadoras += electrodomesticos[i].precioFinal();
            } else if (electrodomesticos[i] instanceof Television) {
                precioTelevisiones += electrodomesticos[i].precioFinal();
            } else {
                precioElectrodomesticos += electrodomesticos[i].precioFinal();
            }
        }

        precioElectrodomesticos += (precioLavadoras + precioTelevisiones);

        System.out.println("Precio lavadoras: " + precioLavadoras);
        System.out.println("Precio televisiones: " + precioTelevisiones);
        System.out.println("Precio electrodomesticos: " + precioElectrodomesticos);

    }
}
