package ejercicio17;

public class Lavadora extends Electrodomestico {
    private static final int CARGA_DEFECTO = 5;
    private int carga;

    public Lavadora(int precioBase, String color, char consumoEnergetico, float peso, int carga) {
        super(precioBase, color, consumoEnergetico, peso);
        this.carga = carga;
    }

    public Lavadora(int precioBase, float peso) {
        super(precioBase, peso);
        this.carga = CARGA_DEFECTO;
    }

    public Lavadora() {
        super();
        this.carga = CARGA_DEFECTO;
    }

    public int getCarga() {
        return carga;
    }

    @Override
    public int precioFinal() {
        return (super.precioFinal() + this.precioCarga());
    }

    private int precioCarga() {
        if (this.getCarga() > 30) {
            return 50;
        } else {
            return 0;
        }
    }
}
