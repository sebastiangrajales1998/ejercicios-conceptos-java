package ejercicio17;

public class Electrodomestico {
    private static final String COLOR_DEFECTO = "blanco";
    private static final char CONSUMO_DEFECTO = 'F';
    private static final int PRECIO_DEFECTO = 100;
    private static final float PESO_DEFECTO = 5.0f;

    protected int precioBase;
    protected String color;
    protected char consumoEnergetico;
    protected float peso;

    public Electrodomestico(int precioBase, String color, char consumoEnergetico, float peso) {
        this.precioBase = precioBase;
        this.color = this.comprobarColor(color);
        this.consumoEnergetico = this.comprobarConsumoEnergetico(consumoEnergetico);
        this.peso = peso;
    }

    public Electrodomestico(int precioBase, float peso) {
        this(precioBase, COLOR_DEFECTO, CONSUMO_DEFECTO, peso);
    }

    public Electrodomestico() {
        this(PRECIO_DEFECTO, PESO_DEFECTO);
    }

    public int getPrecioBase() {
        return precioBase;
    }

    public String getColor() {
        return color;
    }

    public char getConsumoEnergetico() {
        return consumoEnergetico;
    }

    public float getPeso() {
        return peso;
    }

    private char comprobarConsumoEnergetico(char consumoEnergetico) {
        switch (consumoEnergetico) {
            case 'A':
                return 'A';
            case 'B':
                return 'B';
            case 'C':
                return 'C';
            case 'D':
                return 'D';
            case 'E':
                return 'E';
            default:
                return CONSUMO_DEFECTO;
        }
    }

    private String comprobarColor(String color) {
        switch (color.toLowerCase()) {
            case "negro":
                return "negro";
            case "rojo":
                return "rojo";
            case "azul":
                return "azul";
            case "gris":
                return "gris";
            default:
                return COLOR_DEFECTO;
        }
    }

    public int precioFinal() {
        return (this.getPrecioBase() + this.precioConsumo() + this.precioPeso());
    }

    private int precioConsumo() {
        switch (this.getConsumoEnergetico()) {
            case 'A':
                return 100;
            case 'B':
                return 80;
            case 'C':
                return 60;
            case 'D':
                return 50;
            case 'E':
                return 30;
            case 'F':
                return 10;
            default:
                return 0;
        }
    }

    private int precioPeso() {
        if ((this.getPeso() >= 0) && (this.getPeso() <= 19)) {
            return 10;
        } else if ((this.getPeso() >= 20) && (this.getPeso() <= 49)) {
            return 50;
        } else if ((this.getPeso() >= 50) && (this.getPeso() <= 79)) {
            return 80;
        } else if (this.getPeso() >= 80) {
            return 100;
        } else {
            return 0;
        }
    }
}
