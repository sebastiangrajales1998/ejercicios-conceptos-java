package ejercicio17;

public class Television extends Electrodomestico {
    private static final int RESOLUCION_DEFECTO = 20;
    private static final boolean TDT_DEFECTO = false;
    private int resolucion;
    private boolean sintonizadorTDT;

    public Television(int precioBase, String color, char consumoEnergetico, float peso, int resolucion, boolean sintonizadorTDT) {
        super(precioBase, color, consumoEnergetico, peso);
        this.resolucion = resolucion;
        this.sintonizadorTDT = sintonizadorTDT;
    }

    public Television(int precioBase, float peso) {
        super(precioBase, peso);
        this.resolucion = RESOLUCION_DEFECTO;
        this.sintonizadorTDT = TDT_DEFECTO;
    }

    public Television() {
        super();
        this.resolucion = RESOLUCION_DEFECTO;
        this.sintonizadorTDT = TDT_DEFECTO;
    }

    public int getResolucion() {
        return resolucion;
    }

    public boolean isSintonizadorTDT() {
        return sintonizadorTDT;
    }

    @Override
    public int precioFinal() {
        return (super.precioFinal() + this.precioResolucion() + this.precioTDT());
    }

    private int precioResolucion() {
        if (this.getResolucion() > 40) {
            return ((int) (this.getPrecioBase() * 0.3));
        } else {
            return 0;
        }
    }

    private int precioTDT() {
        if (this.isSintonizadorTDT()) {
            return 50;
        } else {
            return 0;
        }
    }
}
