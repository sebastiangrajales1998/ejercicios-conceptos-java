package ejercicio1;

public class Main {
    public static void main(String[] args) {
        /* Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de  los  dos.
           Si  son  iguales  indicarlo también.  Ve  cambiando  los  valores  para comprobar que funciona. */

        final int PRIMER_NUMERO = -550;
        final int SEGUNDO_NUMERO = 350;

        if (PRIMER_NUMERO > SEGUNDO_NUMERO) {
            System.out.println("El primer número es mayor, " + PRIMER_NUMERO + " > " + SEGUNDO_NUMERO);
        } else if (PRIMER_NUMERO < SEGUNDO_NUMERO) {
            System.out.println("El segundo número es mayor, " + PRIMER_NUMERO + " < " + SEGUNDO_NUMERO);
        } else {
            System.out.println("Ambos números son iguales, " + PRIMER_NUMERO + " == " + SEGUNDO_NUMERO);
        }
    }
}
